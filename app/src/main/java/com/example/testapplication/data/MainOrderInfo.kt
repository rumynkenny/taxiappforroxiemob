package com.example.testapplication.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

//@JsonClass(generateAdapter = true)
@Parcelize
data class MainOrderInfo(
    val id: Long,
    val startAddress: AddressWrapper,
    val endAddress: AddressWrapper,
    val price: PriceWrapper,
    val orderTime: String,
    val vehicle: VehicleWrapper
) : Parcelable





