package com.example.testapplication.screens


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.testapplication.data.MainOrderInfo
import com.example.testapplication.databinding.FragmentOrderDetailsBinding
import java.io.File

class OrderDetailsFragment : Fragment() {

    private var _binding: FragmentOrderDetailsBinding? = null

    private val binding: FragmentOrderDetailsBinding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOrderDetailsBinding.inflate(LayoutInflater.from(requireContext()))
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getArgs()
            .let { order ->
                binding.testTV.text = order?.vehicle?.photo
                toBitMap(order?.vehicle?.photo.toString())
            }.let { bitmap ->
                binding.testIV.setImageBitmap(bitmap)
            }
    }

    private fun getArgs(): MainOrderInfo? {
        return arguments?.getParcelable(orderId)
    }

    private fun toBitMap(imageName: String): Bitmap {
        val file =
            File(requireContext().cacheDir, imageName)
        return BitmapFactory.decodeFile(file.path)
    }

    companion object {
        const val orderId = "ORDER_ID_KEY"
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}


