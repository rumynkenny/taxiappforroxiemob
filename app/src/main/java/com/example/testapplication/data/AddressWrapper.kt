package com.example.testapplication.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize


//@JsonClass(generateAdapter = true)
@Parcelize
    data class AddressWrapper(
        val city: String,
        val address: String
    ) : Parcelable
