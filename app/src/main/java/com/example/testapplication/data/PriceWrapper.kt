package com.example.testapplication.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize


//@JsonClass(generateAdapter = true)
@Parcelize
data class PriceWrapper(
    val amount: Int,
    val currency: String
) : Parcelable