package com.example.testapplication.api

import com.example.testapplication.data.MainOrderInfo
import okhttp3.ResponseBody
import retrofit2.http.*

interface ApiService {


    @GET("careers/test/orders.json")
    suspend fun getActiveOrders(): List<MainOrderInfo>

    @GET
    suspend fun getCarPhoto(@Url fileUrl: String): ResponseBody

}
