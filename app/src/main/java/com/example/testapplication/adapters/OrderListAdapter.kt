package com.example.testapplication.adapters


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.testapplication.data.MainOrderInfo
import com.example.testapplication.databinding.ItemOrderBinding
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter


class OrderListAdapter(private val onItemClick: (order: MainOrderInfo) -> Unit) :
    ListAdapter<MainOrderInfo, OrderListAdapter.Holder>(DiffUtilCallback()) {

    private lateinit var binding: ItemOrderBinding

    class Holder(
        private val binding: ItemOrderBinding,
        private val onItemClick: (order: MainOrderInfo) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {


        fun bind(order: MainOrderInfo) {
            with(binding) {
                startAddressTV.text = order.startAddress.let { toAddress(it.city, it.address) }
                endAddressTV.text = order.endAddress.let { toAddress(it.city, it.address) }
                orderPriceTV.text = order.price.let { toPrice(it.amount, it.currency) }
                ordersDataTV.text = order.orderTime.let { toTime(it) }
                root.setOnClickListener {
                    onItemClick(order)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        binding = ItemOrderBinding.inflate(LayoutInflater.from(parent.context))
        return Holder(binding, onItemClick)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))

    }

    class DiffUtilCallback : DiffUtil.ItemCallback<MainOrderInfo>() {
        override fun areItemsTheSame(oldItem: MainOrderInfo, newItem: MainOrderInfo): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: MainOrderInfo,
            newItem: MainOrderInfo
        ): Boolean {
            return oldItem == newItem
        }

    }

    companion object {

        fun toPrice(amount: Int, currency: String): String {
            return "${amount / 100},${amount % 100} $currency"
        }

        fun toAddress(city: String, address: String): String {
            return "$city, $address"
        }

        fun toTime(date:String):String {
            val odt = OffsetDateTime.parse(date)
            val dtf = DateTimeFormatter.ofPattern("MMM:dd, yyyy, HH:mm")
            return dtf.format(odt)
        }
    }

}

