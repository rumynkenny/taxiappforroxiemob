package com.example.testapplication.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create


object Networking {

    private const val BASE_URL = "https://www.roxiemobile.ru"

    private fun okHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addNetworkInterceptor(
                HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY)
            )
            .build()
    }

    private fun retrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .client(okHttpClient())
            .build()
    }

    private val ApiService: ApiService by lazy {
        retrofit().create()
    }

    val RoxieMobileRequester: RoxieMobileRequester by lazy {
        RoxieMobileRequester(ApiService)
    }

}