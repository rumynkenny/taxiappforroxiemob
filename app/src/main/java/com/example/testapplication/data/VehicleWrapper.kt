package com.example.testapplication.data

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize


//@JsonClass(generateAdapter = true)
@Parcelize
data class VehicleWrapper(
    val regNumber: String,
    val modelName: String,
    val photo: String,
    val driverName: String
) : Parcelable