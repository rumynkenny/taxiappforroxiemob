package com.example.testapplication.data.repository


import com.example.testapplication.api.Networking
import com.example.testapplication.data.CarPhoto
import com.example.testapplication.data.MainOrderInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File

class Repository {

    suspend fun getOrderList(): List<MainOrderInfo> {
        return withContext(Dispatchers.IO) {
            Networking.RoxieMobileRequester.getActiveOrders()
        }
    }

    suspend fun getCarPhoto(imageName: String): CarPhoto {
        return withContext(Dispatchers.IO) {
            BASE_PHOTO_PATH.plus(PATH_SEPARATOR).plus(imageName)
                .let { Networking.RoxieMobileRequester.getCarPhoto(it) }
        }
    }

    suspend fun saveInCache(file: File, carPhoto: CarPhoto): Number {
        return withContext(Dispatchers.IO) {
            val outputStream = file.outputStream()
            outputStream.use {
                carPhoto.bytes.copyTo(it)
            }
        }
    }

    fun orderIsActually(file: File): Boolean {
        return file.exists()
    }

    companion object {
        private const val PATH_SEPARATOR = "/"
        private const val BASE_PHOTO_PATH = "/careers/test/images/"
    }

}
