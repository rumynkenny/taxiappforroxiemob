package com.example.testapplication.screens


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testapplication.R
import com.example.testapplication.adapters.OrderListAdapter
import com.example.testapplication.data.MainOrderInfo
import com.example.testapplication.databinding.FragmentMainBinding
import com.example.testapplication.viewmodels.MainFragmentViewModel
import kotlinx.coroutines.*


class MainFragment : Fragment() {

    private val mainViewModel: MainFragmentViewModel by viewModels()

    private var ordersAdapter: OrderListAdapter? = null

    private lateinit var binding: FragmentMainBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(LayoutInflater.from(requireContext()))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        initList()
        binding.getButton.setOnClickListener {
            getOrders()
        }
    }

    private fun initList() {
        ordersAdapter = OrderListAdapter { order ->
            if (mainViewModel.orderIsActually(order)) {
                findNavController()
                    .navigate(
                        R.id.action_mainFragment_to_orderDetailsFragment,
                        bundleOf(OrderDetailsFragment.orderId to order)
                    )
            } else {
                toast("Orders is not actually. Please refresh orders list")
            }
        }
        with(binding) {
            recyclerView.adapter = ordersAdapter
            recyclerView.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observeViewModel() {
        mainViewModel.orders.observe(viewLifecycleOwner) { ordersList ->

            lifecycleScope.async(Dispatchers.Default) { sortedByDate(ordersList) }
                .let {
                    lifecycleScope.launch(Dispatchers.Main) {
                        ordersAdapter?.submitList(it.await())
                    }
                }
            ordersList.forEach { order ->
                lifecycleScope.launch(Dispatchers.IO) {
                    downloadAndSavePhoto(order)
                }
            }
        }
    }

    private fun getOrders() {
        lifecycleScope.launch(Dispatchers.IO) {
            mainViewModel.getOrderList()
        }
    }

    private suspend fun downloadAndSavePhoto(order: MainOrderInfo) {
        mainViewModel.downloadAndSave(order)
    }

    private fun sortedByDate(orders: List<MainOrderInfo>): List<MainOrderInfo> {
        return orders.sortedByDescending {
            it.orderTime
        }
    }

    private fun toast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }


}