package com.example.testapplication.viewmodels

import android.app.Application
import android.os.CountDownTimer
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.*
import com.example.testapplication.data.MainOrderInfo
import com.example.testapplication.data.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes
import java.util.*

class MainFragmentViewModel(application: Application) : AndroidViewModel(application) {


    private val cachePath = getApplication<Application>().cacheDir
    private val repository = Repository()

    private val ordersLiveData = MutableLiveData<List<MainOrderInfo>>()

    val orders: LiveData<List<MainOrderInfo>>
        get() = ordersLiveData


    suspend fun getOrderList() {
        viewModelScope.launch {
            try {
                ordersLiveData.value = repository.getOrderList()
            } catch (e:IOException) {
                toast("Please check network connection")
            }

        }
    }

    suspend fun downloadAndSave(order: MainOrderInfo) {
        val imageName = order.vehicle.photo
        val file = File(cachePath, imageName)
        if (!file.exists()) {
            viewModelScope.launch(Dispatchers.IO) {
                repository.getCarPhoto(order.vehicle.photo)
                    .apply {
                        repository.saveInCache(file, this)
                        viewModelScope.launch(Dispatchers.Main) {
                            timer(file, liveTime)
                        }
                    }
            }
        } else {
            viewModelScope.launch(Dispatchers.Main) {
                currentTimer(file)
            }
        }
    }

    fun orderIsActually(order: MainOrderInfo): Boolean {
        val imageName = order.vehicle.photo
        val file = File(cachePath, imageName)
        return repository.orderIsActually(file)
    }


    private fun currentTimer(file: File) {
        if (file.exists() && getFileLiveTime(file) > liveTime) {
            file.delete()
        } else if (file.exists() && getFileLiveTime(file) < liveTime) {
            timer(file, liveTime.minus(getFileLiveTime(file)))
        }
    }

    private fun timer(file: File, liveTime: Long) {
        object : CountDownTimer(liveTime, 5000) {
            override fun onTick(millisUntilFinished: Long) {
                Log.i("TAG", "onTick: tick ")
                Log.i("TAG", "currentTimer: ${getFileLiveTime(file)}")
            }

            override fun onFinish() {
                file.delete()
                Log.i("TAG", "onFinish: File is deleted ")
            }
        }.start()
    }

    private fun fileCreationTime(file: File): Long {
        val basicFileAttributes = Files.readAttributes(
            file.toPath(),
            BasicFileAttributes::class.java
        )
        return basicFileAttributes.creationTime().toInstant().toEpochMilli()
    }

    private fun getCurrentTime(): Long {
        return Calendar.getInstance().toInstant().toEpochMilli()
    }

    private fun getFileLiveTime(file: File): Long {
        return getCurrentTime().minus(fileCreationTime(file))
    }

    private fun toast(msg: String) {
        return Toast.makeText(getApplication(), msg, Toast.LENGTH_SHORT).show()
    }


    companion object {
        const val liveTime: Long = 1000 * 20
    }

}


