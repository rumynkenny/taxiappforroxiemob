package com.example.testapplication.api

import com.example.testapplication.data.CarPhoto
import com.example.testapplication.data.MainOrderInfo
import okhttp3.ResponseBody

class RoxieMobileRequester(
    private val apiService: ApiService
) {

    suspend fun getActiveOrders(): List<MainOrderInfo> {
        return apiService.getActiveOrders()
    }


    suspend fun getCarPhoto(imageName: String): CarPhoto {
        return map(apiService.getCarPhoto(imageName))
    }

    private fun map(body: ResponseBody): CarPhoto {
        return CarPhoto(body.byteStream())
    }
}