package com.example.testapplication.data

import java.io.InputStream

data class CarPhoto(
    val bytes: InputStream
)
